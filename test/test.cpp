// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"



#include<time.h> 
#include<functional>


#include "BombManCore.h"

Map _m;

BombManAI * _ai1 ,* _ai2;
AIModule _m1, _m2;

#define ONE_ROUND_COMPETITION    200000

int _tmain(int argc, _TCHAR* argv[])
{
	int player1 = 0;
	int player2 = 0;
	{
		_m.load();

		_ai1 = _m1.Load(TEXT("BombManAI1.dll"),_m.get_width(), _m.get_height(),_m.get_creature(0));
		_ai2 = _m2.Load(TEXT("BombManAI2.dll"),_m.get_width(), _m.get_height(),_m.get_creature(1));

		for(int i =0 ;i< ONE_ROUND_COMPETITION; i++)
		{
			if(_ai1)_ai1->next_step();
			if(_ai2)_ai2->next_step();

			_m.ui_update();
		}

		player1 = _m.get_creature(0)->get_dead_count();
		player2 = _m.get_creature(1)->get_dead_count();

		printf("Round 1 player 1 VS player 2 %d:%d\n" ,_m.get_creature(0)->get_dead_count(), _m.get_creature(1)->get_dead_count());
	}

	{
		_m.load();

		_ai1 = _m1.Load(TEXT("BombManAI1.dll"),_m.get_width(), _m.get_height(),_m.get_creature(1));
		_ai2 = _m2.Load(TEXT("BombManAI2.dll"),_m.get_width(), _m.get_height(),_m.get_creature(0));

		for(int i =0 ;i< ONE_ROUND_COMPETITION; i++)
		{
			if(_ai1)_ai1->next_step();
			if(_ai2)_ai2->next_step();

			_m.ui_update();
		}

		player1 += _m.get_creature(1)->get_dead_count();
		player2 += _m.get_creature(0)->get_dead_count();

		printf("Round 2 player 1 VS player 2 %d:%d\n" ,_m.get_creature(1)->get_dead_count(), _m.get_creature(0)->get_dead_count());
	}

	printf("Final player 1 VS player 2 %d:%d\n" ,player1/2, player2/2);

	return 0;
}


