// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "BombMan.h"
#include "ChildView.h"

#define TIMER_GAP_OF_MILISECOND 10
#define TIMER_ID_UPDATE_VIEW 12345

#include "BombManCore.h"
#include "BombManAI.h"
Map _m;
Creature * _c;
BombManAI * _ai1 ,* _ai2;
AIModule _m1, _m2;

#include "BombManCore2.h"
CBombManCore2 bm2;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <time.h>      
long getCurrentTime()    
{    
	return GetTickCount();
   //struct timeval tv;    
   //gettimeofday(&tv,NULL);    
   //return tv.tv_sec * 1000 + tv.tv_usec / 1000;    
}
// CChildView




int update_time = 0;

CChildView::CChildView()
{
	update_timer_hander = 0;
	_m.load();
	_c = _m.get_creature(0);

	_ai1 = _m1.Load(TEXT("BombManAI1.dll"),_m.get_width(), _m.get_height(),_m.get_creature(0));
	_ai2 = _m2.Load(TEXT("BombManAI2.dll"),_m.get_width(), _m.get_height(),_m.get_creature(1));

	bm2.load();
}

CChildView::~CChildView()
{
	KillTimer(update_timer_hander);
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_KEYDOWN()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{
	if(this->IsIconic())return;
	CPaintDC dc(this); // device context for painting


	CRect rc;
	CDC dcMem;
	GetClientRect(&rc);
	CBitmap bmp; //内存中承载临时图象的位图


	dcMem.CreateCompatibleDC(&dc); //依附窗口DC创建兼容内存DC
	//创建兼容位图(必须用pDC创建，否则画出的图形变成黑色)
	bmp.CreateCompatibleBitmap(&dc,rc.Width(),rc.Height());
	CBitmap *pOldBit=dcMem.SelectObject(&bmp);
	//按原来背景填充客户区，不然会是黑色
	dcMem.FillSolidRect(rc,RGB(255,255,255));

		//画图，添加你要画图的代码，不过用dcMem画，而不是pDC；

	{
		// Do not call CWnd::OnPaint() for painting messages
		int static tiled_width = 7;
		int static tiled_height = 7;

		CBrush brush1,brush2,brush3,*oldbrush;
		brush1.CreateSolidBrush(RGB(0,0,34));
		brush2.CreateSolidBrush(RGB(0,23,0));
		brush3.CreateSolidBrush(RGB(144,0,133));

#ifdef ENABLE_BOMBMANCORE
		for(int w = 0; w< _m.m_w ; w++)
		{
			for(int h = 0; h< _m.m_h ; h++)
			{
				switch(_m.m_tiled[w][h].m_p)
				{
				case 0://null
					dcMem.FillSolidRect(w*tiled_width, h*tiled_height, tiled_width, tiled_height, RGB(255,255,255));
					break;
				case 1://block
					dcMem.FillSolidRect(w*tiled_width, h*tiled_height, tiled_width, tiled_height, RGB(0,255,100));
					break;
				case 2://wall
					dcMem.FillSolidRect(w*tiled_width, h*tiled_height, tiled_width, tiled_height, RGB(100,0,0));
					break;
				}
				if(_m.m_tiled[w][h].is_put_bomb())
				{
					oldbrush=dcMem.SelectObject(&brush1);
					dcMem.Ellipse(w*tiled_width, h*tiled_height, w*tiled_width+tiled_width, h*tiled_height+tiled_height);
					dcMem.SelectObject(oldbrush);
				}
				else if(_m.m_tiled[w][h].m_fire)
				{
					dcMem.FillSolidRect(w*tiled_width, h*tiled_height, tiled_width, tiled_height, RGB(255,0,0));
				}
			}
		}

		std::vector<Creature*>::iterator iter = _m.m_creatrues.begin();
		for(;iter!= _m.m_creatrues.end();iter++)
		{
			oldbrush=dcMem.SelectObject(&brush3);
			dcMem.Ellipse((*iter)->get_x()*tiled_width, (*iter)->get_y()*tiled_height, 
				(*iter)->get_x()*tiled_width+tiled_width, (*iter)->get_y()*tiled_height+tiled_height);
			dcMem.SelectObject(oldbrush);
		}

		iter = _m.m_creatrues.begin();
		CString _game_result;
		_game_result.AppendFormat(_T("Updated(%d) "),update_time);
		for(;iter!= _m.m_creatrues.end();iter++)
		{
			_game_result.AppendFormat(_T("Player %d=%d,"),(*iter)->get_index(), (*iter)->get_dead_count());
		}
		oldbrush=dcMem.SelectObject(&brush3);
		dcMem.TextOut(0,_m.get_height()*tiled_height,_game_result);
		dcMem.SelectObject(oldbrush);

		CPoint _p[4];
		_p[0].SetPoint(0,0);
		_p[1].SetPoint(_m.m_w * tiled_width,0);
		_p[2].SetPoint(_m.m_w * tiled_width, _m.m_h * tiled_height);
		_p[3].SetPoint(0,_m.m_h * tiled_height);
		dcMem.Polyline(_p, 4);
#endif

#define BOMBMANCORE2
#ifdef BOMBMANCORE2
		for(int w = 0; w< bm2.get_map_width() ; w++)
		{
			for(int h = 0; h< bm2.get_map_height() ; h++)
			{
				CBombManCore2::BombManTitled titled = bm2.get_titled(w,h);
				switch(titled)
				{
				case CBombManCore2::BM2_none://null
					dcMem.FillSolidRect(w*tiled_width, h*tiled_height, tiled_width, tiled_height, RGB(255,255,255));
					break;
				case CBombManCore2::BM2_bomb:
					{
						oldbrush=dcMem.SelectObject(&brush1);
						dcMem.Ellipse(w*tiled_width, h*tiled_height, w*tiled_width+tiled_width, h*tiled_height+tiled_height);
						dcMem.SelectObject(oldbrush);
					}
					break;
				case CBombManCore2::BM2_firing:
					dcMem.FillSolidRect(w*tiled_width, h*tiled_height, tiled_width, tiled_height, RGB(255,0,0));
					break;
				}
			}
		}
		for(int i=0;i<bm2.get_man_size();i++)
		{
			int x,y;
			bm2.get_man_position(i,x,y);
			oldbrush=dcMem.SelectObject(&brush3);
			dcMem.Ellipse(x*tiled_width, y*tiled_height, 
				x*tiled_width+tiled_width, y*tiled_height+tiled_height);
			dcMem.SelectObject(oldbrush);
		}

		CPoint _p[4];
		_p[0].SetPoint(0,0);
		_p[1].SetPoint(bm2.get_map_width() * tiled_width,0);
		_p[2].SetPoint(bm2.get_map_width() * tiled_width, bm2.get_map_height() * tiled_height);
		_p[3].SetPoint(0,bm2.get_map_height() * tiled_height);
		dcMem.Polyline(_p, 4);
#endif
	}

	dc.BitBlt(0,0,rc.Width(),rc.Height(),&dcMem,0,0,SRCCOPY);

	//将内存DC上的图象拷贝到前台
	//绘图完成后的清理
	dcMem.DeleteDC();     //删除DC
	bmp.DeleteObject(); //删除位图

}

BOOL CChildView::OnEraseBkgnd(CDC*)
{
	return true;
}

void CChildView::OnKeyDown(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	switch(nChar)
	{
	case 'W':
		_c->move_up();
		break;
	case 'S':
		_c->move_down();
		break;
	case 'A':
		_c->move_left();
		break;
	case 'D':
		_c->move_right();
		break;
	case 'L':
		_c->set_bomb();
		break;
	case 'K':
		_c->explode();
		break;
	}
}

void CChildView::OnTimer(UINT nIDEvent)
{
	if(nIDEvent == TIMER_ID_UPDATE_VIEW)
	{
		update_time++;
		if(_ai1)_ai1->next_step();
		if(_ai2)_ai2->next_step();
		_m.ui_update();

		bm2.update();
		this->Invalidate();
	}
}

int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	update_timer_hander = SetTimer(TIMER_ID_UPDATE_VIEW,TIMER_GAP_OF_MILISECOND,0);
	return 0;
}