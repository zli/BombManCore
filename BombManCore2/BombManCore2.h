// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 BOMBMANCORE2_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// BOMBMANCORE2_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
#ifdef BOMBMANCORE2_EXPORTS
#define BOMBMANCORE2_API __declspec(dllexport)
#else
#define BOMBMANCORE2_API __declspec(dllimport)
#endif



// 此类是从 BombManCore2.dll 导出的
class BOMBMANCORE2_API CBombManCore2 {
public:
	CBombManCore2(void);
	~CBombManCore2(void);
	// TODO: 在此添加您的方法。

	enum BombManTitled
	{
		BM2_firing,
		BM2_bomb,
		BM2_none,
	};

	void load();

	void update();

	int get_man_size();
	void get_man_position(int index, int&x,int &y);

	int get_map_width();
	int get_map_height();

	BombManTitled get_titled(int x,int y);

};

extern BOMBMANCORE2_API int nBombManCore2;

BOMBMANCORE2_API int fnBombManCore2(void);

