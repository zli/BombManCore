/*
 * RegionData.h
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#ifndef REGIONDATA_H_
#define REGIONDATA_H_

#include <vector>
#include <string>
#include "json/json.h"
typedef struct Position
{
	int x;
	int y;
	Position(int _x,int _y)
	{
		x = _x;
		y = _y;
	}
	Position(const Position &_pos)
	{
		x = _pos.x;
		y = _pos.y;
	}
	Position()
	{
		x = y = 0;
	}
}Postion;

class RegionData {
public:
	RegionData();
	virtual ~RegionData();
private:
	int height;
	int width;
	int control_size;
	std::vector<Position> positions;
	Json::Value value;
public:
	bool load(std::string match_data_dir);
	bool save(std::string match_data_dir);

	int get_region_height();
	int get_region_width();
	int get_max_control_size();

	Position get_reset_position(int control_index);

	int get_map_data(int _index);


	void set_region_height(int h);
	void set_region_width(int w);
	void set_max_control_size(int s);

	void set_reset_position(int control_index,Position &pos);

	void set_map_data(int _index,int d);
};

#endif /* REGIONDATA_H_ */
