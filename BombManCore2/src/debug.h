#ifndef DEBUG_H_
#define DEBUG_H_
#include <stdlib.h>
#include <stdio.h>

#define ASSERT(a) \
	do { \
		if(!(a)) \
		{ \
			exit(0);\
        } \
	}while(0)


#endif
