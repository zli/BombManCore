/*
 * Titled.cpp
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#include "Titled.h"
#include "debug.h"

Titled::Titled() {
	// TODO Auto-generated constructor stub
	bomb = NULL;
	firing = 0;
}

Titled::~Titled() {
	// TODO Auto-generated destructor stub
	std::set<Man*>::iterator iter = objs.begin();
	for(;iter != objs.end();iter++)
	{
		delete *iter;
	}
	if(bomb)
		delete bomb;
}

void Titled::add_man(Man* obj) {
	objs.insert(obj);
}

void Titled::del_man(Man* obj) {
	std::set<Man*>::iterator iter = objs.find(obj);
	if(iter != objs.end())
	{
		objs.erase(iter);
	}
}

void Titled::update() {
	if(firing > 0) firing--;
	if(bomb) bomb->update();
	std::set<Man*>::iterator iter = objs.begin();
	for(;iter!=objs.end();iter++)
	{
		(*iter)->update();
	}
}

void Titled::draw(int x, int y) {
	if(bomb) bomb->draw(x,y);
	std::set<Man*>::iterator iter = objs.begin();
	for(;iter!=objs.end();iter++)
	{
		(*iter)->draw(x,y);
	}
}

Bomb* Titled::get_bomb() {
	return bomb;
}

void Titled::set_bomb(Bomb* _bomb) {
	bomb = _bomb;
}

void Titled::set_fire() {
	firing = FIRE_LIFE;
	//if(bomb) bomb->set_explode();
	std::set<Man*>::iterator iter = objs.begin();
	for(;iter!=objs.end();iter++)
	{
		(*iter)->die();
	}
}

bool Titled::is_firing()
{
	return firing > 0;
}
