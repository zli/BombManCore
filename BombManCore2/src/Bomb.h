/*
 * Bomb.h
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#ifndef BOMB_H_
#define BOMB_H_

#include "DrawObject.h"

#define BOMB_LIFE  200 //initinal time
#define BOMB_POWER 1 //

class Bomb: public DrawObject {
public:
	Bomb(int _life =BOMB_LIFE ,int _power = BOMB_POWER);
	virtual ~Bomb();
private:
	int life;
	int power;
public:
	void update();
	void draw(int x, int y);

	int get_life_time();

	inline int get_power(){return power;};
	void set_explode();
};

#endif /* BOMB_H_ */
