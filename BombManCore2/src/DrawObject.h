/*
 * DrawObject.h
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#ifndef DRAWOBJECT_H_
#define DRAWOBJECT_H_

class DrawObject {
public:
	DrawObject();
	virtual ~DrawObject();

	virtual void update() = 0;
	virtual void draw(int x, int y) = 0;
};

#endif /* DRAWOBJECT_H_ */
