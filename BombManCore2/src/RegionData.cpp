/*
 * RegionData.cpp
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#include "RegionData.h"
#include <fstream>
//#include <sstream>

RegionData::RegionData() {
	// TODO Auto-generated constructor stub

}

RegionData::~RegionData() {
	// TODO Auto-generated destructor stub
}

bool RegionData::load(std::string match_data_dir) {
	std::ifstream f(match_data_dir.append( "\\map.json").c_str());
	if(f.is_open())
	{
		Json::Reader json_reader;
		value.clear();
		return json_reader.parse(f,value);
	}
	return false;
}

bool RegionData::save(std::string match_data_dir) {
	std::ofstream f(match_data_dir.append( "\\map.json").c_str());
	if(!f.is_open())
	{
		//Json::StyledStreamWriter json_writer;
		//json_writer.write(f,value);
		return false;
	}
	
	Json::FastWriter json_writer;
	f<<json_writer.write(value);

	return true;
}

int RegionData::get_region_height() {
	Json::Value j_h;
	j_h = value.get("height", j_h);
	if(j_h.isInt())
	{
		return j_h.asInt();
	}
	return 0;
}

int RegionData::get_region_width() {
	Json::Value j_h;
	j_h = value.get("width", j_h);
	if(j_h.isInt())
	{
		return j_h.asInt();
	}
	return 0;
}

int RegionData::get_max_control_size() {
	Json::Value j_h;
	j_h = value.get("control_size", j_h);
	if(j_h.isInt())
	{
		return j_h.asInt();
	}
	return 0;
}

Position RegionData::get_reset_position(int control_index) {
	Json::Value j_h;
	j_h = value.get("reset_position", j_h);
	if(j_h.isArray())
	{
		return Position( j_h[control_index*2].asInt(),j_h[control_index*2+1].asInt());
	}
	return Position(0,0);
}


void RegionData::set_region_height(int h) {
	value["height"] = h;
}

void RegionData::set_region_width(int w) {
	value["width"] = w;
}

void RegionData::set_max_control_size(int s) {
	value["control_size"] = s;
}

void RegionData::set_reset_position(int control_index,Position & pos) {
	//Json::Value j_h;
	//j_h[control_index*2] = pos.x;
	//j_h[control_index*2+1] = pos.y;
	if(value["reset_position"].size() < control_index*2+1)
		value["reset_position"].resize(control_index*2+1);
	value["reset_position"][control_index*2] = pos.x;
	value["reset_position"][control_index*2+1] = pos.y;
}


void RegionData::set_map_data(int _index,int d) {
	//Json::Value j_h;
	//j_h[control_index*2] = pos.x;
	//j_h[control_index*2+1] = pos.y;
	if(value["map_data"].size() < _index)
		value["map_data"].resize(_index);
	value["map_data"][_index] = d;
}

int RegionData::get_map_data(int _index) {
	//Json::Value j_h;
	//j_h = value.get("map_data", j_h);
	//if(j_h.isArray())
	//{
	//	return j_h[_index].asInt();
	//}

	return value["map_data"][_index].asInt();
}