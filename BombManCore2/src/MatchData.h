/*
 * FileManControl.h
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#ifndef MatchData_H_
#define MatchData_H_

#include <list>
#include <string>
#include "RegionData.h"

enum Action
{
	action_stand,
	action_move_left,
	action_move_right,
	action_move_up,
	action_move_down,
	action_move_left_with_bomb,
	action_move_right_with_bomb,
	action_move_up_with_bomb,
	action_move_down_with_bomb,
	action_reset //maybe dead
};


class MatchData: public RegionData{
public:
	MatchData();
	virtual ~MatchData();
private:

public:
	virtual int get_data_length()=0;
	virtual int get_control_size()=0;

	virtual Action get_data(int control_index, int tick)=0;
};


class FileMatchData:public MatchData{
public:
	Json::Value value;
	/**
	 * map data file, action data file local in this directory
	 */
	bool load(std::string match_data_dir);

		/**
	 * map data file, action data file local in this directory
	 */
	bool save(std::string match_data_dir);

	int get_data_length();
	int get_control_size();

	Action get_data(int control_index, int tick);

	/*
	* control_index base on 0, tick base on 0
	*/
	void set_data(int control_index,Action a, int tick);

	void set_data_length(int length);
	void set_control_size(int s);
};

class AIModulesMatchData:public MatchData{
public:
	/**
	 * map data file, and AI module local at this directory
	 */
	bool load(std::string match_data_dir);
};

#endif /* FILEMANCONTROL_H_ */
