/*
 * Titled.h
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#ifndef TITLED_H_
#define TITLED_H_

#include <set>
#include "Man.h"
#include "Bomb.h"

#define FIRE_LIFE 2

class Titled: public DrawObject {
public:
	Titled();
	virtual ~Titled();
private:
	Bomb * bomb;
	std::set<Man*> objs;
	int firing;//only for drawing fire
public:
	void add_man(Man* obj);
	void del_man(Man* obj);

public:
	void update();
	void draw(int x, int y);

	Bomb* get_bomb();
	void set_bomb(Bomb* _bomb);
	void set_fire();
	bool is_firing();
};

#endif /* TITLED_H_ */
