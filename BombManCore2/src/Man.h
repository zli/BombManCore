/*
 * Man.h
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#ifndef MAN_H_
#define MAN_H_

#include "DrawObject.h"

class Man: public DrawObject {
private:
	bool is_dead;
public:
	Man();
	virtual ~Man();
public:
	void update();
	void draw(int x, int y);
	inline void die(){is_dead = true;};
	inline bool is_die(){return is_dead;};
	inline void reborn(){is_dead = false;};
};

#endif /* MAN_H_ */
