/*
 * Bomb.cpp
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#include "Bomb.h"
#include <stdio.h>
Bomb::Bomb(int _life,int _power):
	life(_life),
	power(_power)
{
	// TODO Auto-generated constructor stub
	//life = BOMB_LIFE;
	//power = BOMB_POWER;
}

Bomb::~Bomb() {
	// TODO Auto-generated destructor stub
}

void Bomb::update() {
	if(life > 0) life--;
}

void Bomb::draw(int x, int y) {
	printf("bomb (%d,%d)\n",x,y);
}

int Bomb::get_life_time() {
	return life;
}

void Bomb::set_explode() {
	life = 0;
}
