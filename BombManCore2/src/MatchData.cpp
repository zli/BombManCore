/*
 * FileManControl.cpp
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#include "MatchData.h"
#include "debug.h"
#include <fstream>
//#include <sstream>
MatchData::MatchData() {
	// TODO Auto-generated constructor stub

}

MatchData::~MatchData() {
	// TODO Auto-generated destructor stub
}




int FileMatchData::get_data_length() {
	Json::Value j_h;
	j_h = value.get("data_length", j_h);
	if(j_h.isInt())
	{
		return j_h.asInt();
	}
	return 0;
}

int FileMatchData::get_control_size() {
	Json::Value j_h;
	j_h = value.get("control_size", j_h);
	if(j_h.isInt())
	{
		return j_h.asInt();
	}
	return 0;
}


Action FileMatchData::get_data(int control_index, int tick) {

	ASSERT(tick <= get_data_length());
	return (Action)value["control_data"][control_index][tick].asInt();

#if 0 //it's very slow to save to stack value j_h
	Json::Value j_h;
	j_h = value.get("control_data", j_h);
	if(j_h.isArray())
	{
		if(j_h[control_index].isArray())
			return (Action)j_h[control_index][tick].asInt();
	}
	return action_stand;
#endif
#if 0
	if(tick >= 40)
	{
		Action a[]=
		{
			action_stand,
			action_move_left,
			action_move_right,
			action_move_up,
			action_move_down,
			action_move_left,
			action_move_right,
			action_move_up,
			action_move_down,
			action_move_left,
			action_move_right,
			action_move_up,
			action_move_down,
			action_move_right,
			action_move_up,
			action_move_down,
			action_move_left,
			action_move_right,
			action_move_up,
			action_move_down,
			action_move_left,
			action_move_right,
			action_move_up,
			action_move_down,
			action_move_left_with_bomb,
			//action_reset //maybe dead
		};
		return a[rand()%25];
	}
	Action a[2][40]=
	{
		{
			action_move_right,
				action_move_down,
				action_move_right,
				action_move_down,
				action_move_left,
				action_move_down_with_bomb,
				action_move_right_with_bomb,
				action_move_right_with_bomb,
				action_move_up_with_bomb,
				action_reset, //go to reborn position
				action_move_right,
				action_move_down,
				action_move_right,
				action_move_down,
				action_move_left,
				action_move_down_with_bomb,
				action_move_right_with_bomb,
				action_move_right_with_bomb,
				action_move_up_with_bomb,
				action_move_right,

				action_move_down,
				action_move_right,
				action_move_down,
				action_move_left,
				action_move_down_with_bomb,
				action_move_right_with_bomb,
				action_move_right_with_bomb,
				action_move_up_with_bomb,
				action_move_right,

				action_move_down,
				action_move_right,
				action_move_down,
				action_move_left,
				action_move_down_with_bomb,
				action_move_right_with_bomb,
				action_move_right_with_bomb,
				action_move_up_with_bomb,
				action_move_right,
		},
		{
			action_stand,
				action_move_left,
				action_move_right,
				action_move_up,
				action_move_down,
				action_move_left,
				action_move_right_with_bomb,
				action_move_up,
				action_move_down_with_bomb,
				action_reset, //go to reborn position
				action_stand,
				action_move_left,
				action_move_right,
				action_move_up,
				action_move_down,
				action_move_left_with_bomb,
				action_move_right_with_bomb,
				action_move_up_with_bomb,
				action_move_down_with_bomb,
				action_reset, //go to reborn position
				action_stand,
				action_move_left,
				action_move_right,
				action_move_up,
				action_move_down,
				action_move_left_with_bomb,
				action_move_right_with_bomb,
				action_move_up_with_bomb,
				action_move_down_with_bomb,
				action_reset, //go to reborn position
				action_stand,
				action_move_left,
				action_move_right,
				action_move_up,
				action_move_down,
				action_move_left_with_bomb,
				action_move_right_with_bomb,
				action_move_up_with_bomb,
				action_move_down_with_bomb,
				action_reset, //go to reborn position
			}
	};
	return a[control_index][tick];
#endif
}



bool FileMatchData::load(std::string match_data_dir)
{
	if(RegionData::load(match_data_dir)== false)
		return false;

	std::ifstream f(match_data_dir.append( "\\match.json").c_str());
	if(!f.is_open())
		return false;

	Json::Reader json_reader;
	value.clear();
	if(json_reader.parse(f,value) == false)
		return false;

	if(RegionData::get_max_control_size() < get_control_size())
		return false;
	return true;
}

bool FileMatchData::save(std::string match_data_dir)
{
	if(RegionData::save(match_data_dir) == false)
		return false;
	std::ofstream f(match_data_dir.append( "\\match.json").c_str());
	if(!f.is_open())
		return false;

	Json::FastWriter json_writer;
	f<<json_writer.write(value);
	return true;
}


void FileMatchData::set_data(int control_index,Action a, int tick)
{
	if(value["control_data"].size() < control_index+1)
		value["control_data"].resize(control_index+1);
	if(value["control_data"][control_index].size() < tick+1)
		value["control_data"][control_index].resize(tick+1);
	value["control_data"][control_index][tick] = a;
}

void FileMatchData::set_data_length(int length)
{
	value["data_length"] = length;
}
void FileMatchData::set_control_size(int s)
{
	value["control_size"] = s;
}




bool AIModulesMatchData::load(std::string match_data_dir)
{
	if(RegionData::load(match_data_dir)== false)
		return false;



	return true;
}
