/*
 * MatchRegion.h
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#ifndef MATCHREGION_H_
#define MATCHREGION_H_

#include <vector>
#include <string>
#include <list>
#include <map>
#include "Titled.h"
#include "MatchData.h"
#include "Man.h"

class MatchRegion: public DrawObject {
public:
	MatchRegion(MatchData & matchdata);
	virtual ~MatchRegion();
private:
	typedef struct ManRef
	{
		Position pos;
		Man* man;
		ManRef(Man* _man,Position&_pos)
		{
			man = _man;
			pos = _pos;
		}
		ManRef()
		{
			man = NULL;
		}
	}ManRef;
	typedef struct BombRef
	{
		Position pos;
		Bomb* bomb;
		BombRef(Bomb* _bomb,Position&_pos)
		{
			bomb = _bomb;
			pos = _pos;
		}
		BombRef()
		{
			bomb = NULL;
		}
	}BombRef;

	int time_count;

	bool init_region();
	void free_region();

	bool move_man(int index, Position &pos);
	bool set_bomb(Position& pos);
	void check_explode();
	void check_detonate(Bomb* bomb, Position & pos);

	bool move_up(Position & pos);
	bool move_down(Position & pos);
	bool move_right(Position & pos);
	bool move_left(Position & pos);
public:

	std::vector<ManRef> manref;//set public for drawing
	std::list<BombRef> bombref;//set public for drawing
	MatchData& matchdata;//set public for drawing
	Titled **titleds;//set public for drawing

	void play();

	void stop();

	void update();

	void draw(int x, int y);

};

#endif /* MATCHREGION_H_ */
