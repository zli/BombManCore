/*
 * MatchRegion.cpp
 *
 *  Created on: Sep 16, 2015
 *      Author: root
 */

#include "MatchRegion.h"
#include "Bomb.h"

MatchRegion::MatchRegion(MatchData & matchdata)
	:matchdata(matchdata)
{
	// TODO Auto-generated constructor stub

	titleds = NULL;
	time_count = 0;
}

MatchRegion::~MatchRegion() {
	// TODO Auto-generated destructor stub
	free_region();
}

void MatchRegion::play() {
	if(init_region()==false)
		return;
	manref.resize(matchdata.get_control_size());
	for(int i = 0 ; i < matchdata.get_control_size();i++)
	{
		Man * man = new Man();
		manref[i].man = man;
		manref[i].pos.x = 0;
		manref[i].pos.y = 0;
		Position pos = matchdata.get_reset_position(i);
		move_man(i, pos);
	}

	//create timer for updating
}

bool MatchRegion::init_region() {
	free_region();
	int w = matchdata.get_region_width();
	int h = matchdata.get_region_height();
	titleds = new Titled*[h];
	if(titleds)
	{
		int i = 0;
		for(; i< h;i++)
		{
			titleds[i] = new Titled[w];
			if(!titleds[i])
				break;
		}
		if(i != h)
		{
			while(--i >= 0)
			{
				delete []titleds[i];
			}
			delete []titleds;
			titleds = NULL;
			return false;
		}
		for(i=0;i< h;i++)
			for(int j=0;j< w;j++)
			{
				if(matchdata.get_map_data(i*w +j) == 1)
				{
					set_bomb(Position(i,j));
				}
			}
		return true;
	}
	return false;
}

void MatchRegion::free_region() {
	if(titleds)
	{
		for(int i = 0; i< matchdata.get_region_height();i++)
		{
			delete []titleds[i];
		}
		delete []titleds;
		titleds = NULL;
	}
}

bool MatchRegion::move_man(int index, Position &pos) {
	Position _pos = manref[index].pos;
	Man * man = manref[index].man;
	if(titleds[pos.y][pos.x].get_bomb() != NULL)
		return false;
	titleds[_pos.y][_pos.x].del_man(man);

	//pos = matchdata.get_reset_position(index);
	titleds[pos.y][pos.x].add_man(man);
	manref[index].pos = pos;
	return true;
}

void MatchRegion::stop() {
	//kill timer to stop update
}

bool MatchRegion::set_bomb(Position& pos) {
	if(titleds[pos.y][pos.x].get_bomb() != NULL)
		return false;
	Bomb *bomb = new Bomb(rand()%200, rand()%10);
	titleds[pos.y][pos.x].set_bomb(bomb);
	bombref.push_back(BombRef(bomb,pos));
	return true;
}

void MatchRegion::check_detonate(Bomb* bomb, Position & pos) {
	int power = bomb->get_power();
	//check up
	{
		Position _pos = pos;
		for(int i=0;i<power;i++)
		{
			if(move_up(_pos))
			{
				Bomb * _bomb = titleds[_pos.y][_pos.x].get_bomb();
				if(_bomb!=NULL)
				{
					if(_bomb->get_life_time() > 0)
					{
						bomb->set_explode();
						check_detonate(_bomb, _pos);
					}
				}
				titleds[_pos.y][_pos.x].set_fire();
			}
			else
				break;//cannot move
		}
	}
	//check down
	{
		Position _pos = pos;
		for(int i=0;i<power;i++)
		{
			if(move_down(_pos))
			{
				Bomb * _bomb = titleds[_pos.y][_pos.x].get_bomb();
				if(_bomb!=NULL)
				{
					if(_bomb->get_life_time() > 0)
					{
						bomb->set_explode();
						check_detonate(_bomb, _pos);
					}
				}
				titleds[_pos.y][_pos.x].set_fire();
			}
			else
				break;//cannot move
		}
	}
	//check right
	{
		Position _pos = pos;
		for(int i=0;i<power;i++)
		{
			if(move_right(_pos))
			{
				Bomb * _bomb = titleds[_pos.y][_pos.x].get_bomb();
				if(_bomb!=NULL)
				{
					if(_bomb->get_life_time() > 0)
					{
						bomb->set_explode();
						check_detonate(_bomb, _pos);
					}
				}
				titleds[_pos.y][_pos.x].set_fire();
			}
			else
				break;//cannot move
		}
	}
	//check left
	{
		Position _pos = pos;
		for(int i=0;i<power;i++)
		{
			if(move_left(_pos))
			{
				Bomb * _bomb = titleds[_pos.y][_pos.x].get_bomb();
				if(_bomb!=NULL)
				{
					if(_bomb->get_life_time() > 0)
					{
						bomb->set_explode();
						check_detonate(_bomb, _pos);
					}
				}
				titleds[_pos.y][_pos.x].set_fire();
			}
			else
				break;//cannot move
		}
	}
}

void MatchRegion::check_explode() {
	//check detonate for each of bomb
	std::list<BombRef>::iterator iter = bombref.begin();
	for(;iter!=bombref.end();iter++)
	{
		if(iter->bomb->get_life_time() <= 0)
		{
			check_detonate(iter->bomb, iter->pos);
		}
	}

	//clean up exploded bomb
	iter = bombref.begin();
	for(;iter!=bombref.end();)
	{
		if(iter->bomb->get_life_time() <= 0)
		{
			titleds[iter->pos.y][iter->pos.x].set_bomb(NULL);
			delete iter->bomb;
			iter = bombref.erase(iter);
		}
		else
			iter++;
	}
	//reset the dead man
	for(int control_index = 0 ; control_index < matchdata.get_control_size();control_index++)
	{
		if(manref[control_index].man->is_die() == true)
		{
			Position pos = matchdata.get_reset_position(control_index);
			move_man(control_index, pos);
			manref[control_index].man->reborn();
		}
	}
}

void MatchRegion::update() {
	if(time_count > matchdata.get_data_length())
	{
		stop();
		return;
	}
	for(int control_index = 0 ; control_index < matchdata.get_control_size();control_index++)
	{
		Action action = matchdata.get_data(control_index,time_count);
		switch(action)
		{
		case action_stand:
		{
			break;
		}
		case action_move_left:
		{
			Position _pos = manref[control_index].pos;
			if(move_left(_pos))
				move_man(control_index, _pos);
			break;
		}
		case action_move_right:
		{
			Position _pos = manref[control_index].pos;
			if(move_right(_pos))
				move_man(control_index, _pos);
			break;
		}
		case action_move_up:
		{
			Position _pos = manref[control_index].pos;
			if(move_up(_pos))
				move_man(control_index, _pos);
			break;
		}
		case action_move_down:
		{
			Position _pos = manref[control_index].pos;
			if(move_down(_pos))
				move_man(control_index, _pos);
			break;
		}
		case action_move_left_with_bomb:
		{
			Position _pos = manref[control_index].pos;
			if(move_left(_pos))
				move_man(control_index, _pos);
			set_bomb(_pos);
			break;
		}
		case action_move_right_with_bomb:
		{
			Position _pos = manref[control_index].pos;
			if(move_right(_pos))
				move_man(control_index, _pos);
			set_bomb(_pos);
			break;
		}
		case action_move_up_with_bomb:
		{
			Position _pos = manref[control_index].pos;
			if(move_up(_pos))
				move_man(control_index, _pos);
			set_bomb(_pos);
			break;
		}
		case action_move_down_with_bomb:
		{
			Position _pos = manref[control_index].pos;
			if(move_down(_pos))
				move_man(control_index, _pos);
			set_bomb(_pos);
			break;
		}
		case action_reset:
		{
			Position pos = matchdata.get_reset_position(control_index);
			move_man(control_index, pos);
			break;
		}
		}
	}

	check_explode();

	for(int i = 0; i< matchdata.get_region_height();i++)
	{
		for(int j = 0; j< matchdata.get_region_width();j++)
		{
			titleds[i][j].update();
		}
	}

	time_count++;
}

bool MatchRegion::move_up(Position& pos) {
	if(pos.y <= 0)
		return false;
	pos.y--;
	return true;
}

bool MatchRegion::move_down(Position& pos) {
	if(pos.y >= matchdata.get_region_height()-1)
		return false;
	pos.y++;
	return true;
}

bool MatchRegion::move_right(Position& pos) {
	if(pos.x >= matchdata.get_region_width()-1)
		return false;
	pos.x++;
	return true;
}

bool MatchRegion::move_left(Position& pos) {
	if(pos.x <= 0)
		return false;
	pos.x--;
	return true;
}

void MatchRegion::draw(int x, int y) {
	for(int i = 0; i< matchdata.get_region_height();i++)
	{
		for(int j = 0; j< matchdata.get_region_width();j++)
		{
			titleds[i][j].draw(x+i,y+j);
		}
	}
}
