// BombManCore2.cpp : 定义 DLL 应用程序的导出函数。
//
#include "stdafx.h"
#include "BombManCore2.h"


// 这是导出变量的一个示例
BOMBMANCORE2_API int nBombManCore2=0;

// 这是导出函数的一个示例。
BOMBMANCORE2_API int fnBombManCore2(void)
{
	return 42;
}

#include "MatchRegion.h"
FileMatchData data;
MatchRegion *region;
// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 BombManCore2.h
CBombManCore2::CBombManCore2()
{
	region = NULL;
	return;
}

CBombManCore2::~CBombManCore2()
{
	if(region)
	{
		delete region;
		region = NULL;
	}
	return;
}


void CBombManCore2::load()
{
#if 1
	data.set_region_height(50);
	data.set_region_width(50);

	//set map data
	for(int i=0;i< 50 * 50;i++)
	{
		data.set_map_data(i,rand()%25 == 0 ? 1: 0);
	}


	data.set_max_control_size(10);
	data.set_reset_position(9,Position(2,5));
	data.set_reset_position(1,Position(49,49));
	data.set_reset_position(3,Position(2,7));
	data.set_reset_position(2,Position(2,48));
	data.set_reset_position(4,Position(2,9));
	data.set_reset_position(5,Position(2,9));
	data.set_reset_position(6,Position(4,5));
	data.set_reset_position(7,Position(3,5));
	data.set_reset_position(8,Position(2,5));
	data.set_reset_position(0,Position(1,1));


	data.set_control_size(2);

	int data_length = 10000;
	data.set_data_length(data_length);

	for(int i=0;i<data_length;i++)
	{
		data.set_data(0,static_cast<Action>(rand()%10),i);
		data.set_data(1,static_cast<Action>(rand()%10),i);
	}

	data.save(".");
#endif
	data.load(".");


	if(region)
		delete region;
	region = new MatchRegion(data);

	
	region->play();
}

void CBombManCore2::update()
{
	if(region)
		region->update();
}


int CBombManCore2::get_man_size()
{
	return region->manref.size();
}
void CBombManCore2::get_man_position(int index, int&x,int &y)
{
	if(region->manref.size() <= index || index < 0)
		return;
	x = region->manref[index].pos.x;
	y = region->manref[index].pos.y;
}

int CBombManCore2::get_map_width()
{
	return region->matchdata.get_region_width();
}
int CBombManCore2::get_map_height()
{
	return region->matchdata.get_region_height();
}

CBombManCore2::BombManTitled CBombManCore2::get_titled(int x,int y)
{
	if(x < 0 || x >= get_map_width())
		return BM2_none;
	if(y < 0 || y >= get_map_height())
		return BM2_none;

	if(region->titleds[y][x].is_firing())
		return BM2_firing;

	if(region->titleds[y][x].get_bomb())
		return BM2_bomb;

	return BM2_none;
}