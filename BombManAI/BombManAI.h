// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the BOMBMANAI_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// BOMBMANAI_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#pragma once

#ifdef BOMBMANAI_EXPORTS
#define BOMBMANAI_API __declspec(dllexport)
#else
#define BOMBMANAI_API __declspec(dllimport)
#endif


#include "BombManCore.h"
extern "C" BOMBMANAI_API BombManAI * CreateBombManAI(int _map_width,int _map_height,Creature * _c);