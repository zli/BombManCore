// BombManAI.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "BombManAI.h"


#ifdef _MANAGED
#pragma managed(push, off)
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
					  DWORD  ul_reason_for_call,
					  LPVOID lpReserved
					  )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif


// This is the constructor of a class that has been exported.
// see BombManAI.h for the class definition


class  MyBombManAI : public BombManAI
{
	enum Direction
	{
		Direction_up,
		Direction_down,
		Direction_left,
		Direction_right,
		Direction_null
	};

	Direction m_go_to;
public:
	MyBombManAI(int _map_width,int _map_height,Creature * _c)
		:BombManAI(_map_width, _map_height, _c)
	{
		m_go_to = Direction_null;
	}
	~MyBombManAI()
	{
	}
	void next_step();

};

extern "C"  BOMBMANAI_API BombManAI * CreateBombManAI(int _map_width,int _map_height,Creature * _c)
{
	return new MyBombManAI(_map_width, _map_height, _c);
}

void MyBombManAI::next_step()
{
	Radar::Detected up = man->detect_up();
	Radar::Detected down = man->detect_down();
	Radar::Detected left = man->detect_left();
	Radar::Detected right = man->detect_right();

	if(up.obj == Radar::Detected_Bomb || down.obj == Radar::Detected_Bomb)
	{
		if(right.distance > 1 && m_go_to != Direction_left)
		{
			man->move_right();
			m_go_to = Direction_right;
			return;
		}
		else if(left.distance > 1 && m_go_to != Direction_right)
		{
			man->move_left();
			m_go_to = Direction_left;
			return;
		}
	}
	else if(right.obj == Radar::Detected_Bomb || left.obj == Radar::Detected_Bomb)
	{
		if(down.distance > 1 && m_go_to != Direction_up)
		{
			man->move_down();
			m_go_to = Direction_down;
			return;
		}
		else if(up.distance > 1 && m_go_to != Direction_down)
		{
			man->move_up();
			m_go_to = Direction_up;
			return;
		}
	}
	else
	{
	}
#if 1
	int p = rand() % 18;
	switch(p)
	{
	case 0:
	case 5:
	case 6:
	case 13:
	case 14:
		man->move_down();
		break;
	case 1:
	case 7:
	case 8:
	case 15:
	case 16:
		man->move_up();
		break;
	case 2:
	case 9:
	case 10:
	case 17:
	case 18:
		man->move_right();
		break;
	case 3:
	case 11:
	case 12:
	case 19:
	case 20:
		man->move_left();
		break;
	case 4:
		man->set_bomb();
		break;
	}
#endif
}